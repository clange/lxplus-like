# Additional notes

## cloud-init

cloud-init version on CC7 is 19.4 and 21.1 for Alma8 and 9.
ansible_cc is only available since cloud-init release 22.3.

## Home directories

User home seems to be `/home/$USER` instead of in `/afs/cern.ch`
for Alma8 and 9. It's in AFS for CC7.
