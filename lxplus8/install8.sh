#!/bin/sh
# Pass this as --user-data to the openstack server create command
# alternatively the script can be uploaded via the configuration tab
# of the "Launch Instance" panel of OpenStack
/usr/bin/dnf update -y
/usr/bin/dnf install -y tmux zsh
/usr/bin/dnf install -y dnf-autoupdate
/usr/bin/dnf -y install locmap-release
/usr/bin/dnf -y install locmap
for i in afs chrony cvmfs eosclient kerberos lpadmin postfix ssh sudo; do
  locmap --enable $i
done
locmap --configure all
/usr/bin/dnf -y install \
  apptainer \
  bc \
  binutils \
  bzip2 \
  bzip2-libs \
  CERN-CA-certs \
  cern-wrappers \
  cmake3 \
  e2fsprogs \
  e2fsprogs-libs \
  file \
  file-libs \
  gcc \
  gcc-c++ \
  gfal2-util \
  gfal2-all \
  git \
  glibc-devel \
  glibc-devel.i686 \
  glibc-headers \
  glx-utils \
  hostname \
  java-1.8.0-openjdk \
  java-1.8.0-openjdk-devel \
  krb5-devel \
  krb5-workstation \
  libaio \
  libgfortran \
  libSM \
  libX11 \
  libX11-devel \
  libXcursor \
  libXext \
  libXext-devel \
  libXft \
  libXft-devel \
  libXi \
  libXinerama \
  libXmu \
  libXpm \
  libXpm-devel \
  libXrandr \
  libXrender \
  make \
  mesa-dri-drivers \
  mesa-libGL-devel \
  mesa-libGLU-devel \
  nano \
  nspr \
  nss \
  nss-devel \
  nss-util \
  openssl \
  openssl-devel \
  patch \
  pciutils \
  perl-CGI \
  perl-Data-Dumper \
  perl-DBI \
  perl-Env \
  perl-ExtUtils-Embed \
  perl-libwww-perl \
  perl-Storable \
  perl-Switch \
  perl-Thread-Queue \
  perl-YAML \
  podman \
  popt \
  popt-devel \
  python3-pip \
  readline \
  readline-devel \
  strace \
  svn \
  tar \
  tcl \
  tcl-devel \
  time \
  tk \
  tk-devel \
  unzip \
  voms-clients-cpp \
  wget \
  which \
  xrootd-client \
  zip \
  zsh

rpm --import https://repo.opensciencegrid.org/osg/RPM-GPG-KEY-OSG-2
/usr/bin/dnf install -y https://repo.opensciencegrid.org/osg/3.6/osg-3.6-el8-release-latest.rpm
/usr/bin/dnf install -y osg-ca-certs
rpm --import https://linuxsoft.cern.ch/wlcg/RPM-GPG-KEY-wlcg
/usr/bin/dnf install -y https://linuxsoft.cern.ch/wlcg/centos8/x86_64/wlcg-repo-1.0.0-1.el8.noarch.rpm
/usr/bin/dnf install -y HEP_OSlibs wlcg-voms-cms

for i in $(cat /etc/passwd |grep "/home/" | cut -d: -f1); do
  usermod -d /afs/cern.ch/user/$(printf '%c' "$i")/$i $i
done
cern-get-keytab
sed -i.bak '/^#GSSAPIKeyExchange.*/a GSSAPIKeyExchange = yes' /etc/ssh/sshd_config
systemctl restart sshd

curl -L -o cvmfs.tar.gz 'https://gitlab.cern.ch/clange/lxplus-like/-/archive/master/lxplus-like-master.tar.gz?path=cvmfs'
tar xzf cvmfs.tar.gz
rsync -r lxplus-like-master-cvmfs/cvmfs /etc
rm -rf lxplus-like-master-cvmfs cvmfs.tar.gz
systemctl restart autofs

systemctl enable --now dnf-automatic.timer

sed -i.bak '/^#ignore_chown_errors.*/a ignore_chown_errors = "true"' /etc/containers/storage.conf
sed -i.bak '/^# rootless_storage_path.*/a rootless_storage_path = "/tmp/${USER}/containers"' /etc/containers/storage.conf