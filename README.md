# lxplus-like

Ansible playbooks and setup scripts for an LXPLUS-like configuration of
[CERN OpenStack VMs](https://openstack.cern.ch/) (and CERN Linux machines).

If you have not yet used CERN OpenStack, please see the documentation in the
[CERN CloudDocs](https://clouddocs.web.cern.ch/tutorial/index.html)
on how to get started.

You need to be able to connect to the VMs using SSH, i.e. the host key
you registered when creating the VMs needs to be used to connect.

[[_TOC_]]

## Introduction

There are two ways in which the machines can be bootstrapped:

1. by providing a user-data script at creation time,
1. by using Ansible playbooks after creation.

The option providing user-data at creation time is a single command,
but gives less control in case things go wrong.
Nevertheless, the user-data option is the recommended one.
The Ansible playbooks can also be used to bootstrap actual PCs with a
CERN Linux installation.

## Ansible

### Ansible prerequisites

You need to have Ansible installed on the machine on which you are running
the playbooks. See their
[installation instructions](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html). For installation on MacOS, `brew` is easiest:

```shell
brew install ansible
```

### SSH setup

In case you are outside the CERN network, make sure to have an entry as below
in your `~/.ssh/config` (here the VMs are all called `clangevm-*`):

```config
Host clangevm-*.cern.ch
  User root
  IdentityFile ~/.ssh/id_rsa
  ProxyCommand ssh lxplus.cern.ch nc %h 22
```

as well as set the user for LXPLUS:

```config
Host lxplus.cern.ch
  User YOUR_CERN_USERNAME
```

### Ansible installation instructions

Adjust the [`hosts`](./hosts) file by adding the hostnames of your VMs.

Then run the [`bootstrap-simple.yaml`](./bootstrap-simple.yaml) playbook if you have never
before connected to your VMs:

```shell
ansible-playbook bootstrap-simple.yaml
```

This will connect to the hosts, create an SSH host key for each machine,
and add them to your `~/.ssh/known_hosts` file.

If this is causing problems, e.g. if you have recreated a VM and it has now
a new SSH host key, run the [`bootstrap.yaml`](./bootstrap.yaml) playbook
instead or afterwards:

```shell
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook bootstrap.yaml
```

You can run a simple check if communication is working using the
[`ping.yaml`](./ping.yaml) playbook:

```shell
ansible-playbook ping.yaml
```

To install an LXPLUS-like environment and enable CVMFS, AFS, EOS, your own
user account etc.:

```shell
ansible-playbook setup.yaml
```

The playbook will automatically detect which operating system (CC7, Alma8,
Alma9) is installed.

## Post-installation

Make sure to change the SSH config above to use your LXPLUS username
instead of `root`.

## Providing a user-data script

When creating a new VM in [CERN OpenStack](https://openstack.cern.ch/), one
can provide a user-data script to run additional commands after the machine
has been created. The script provided here perform the same actions as the
Ansible scripts.

You can download the scripts and provide them when launching an instance from
the project's [compute site](https://openstack.cern.ch/project/):

- `install7.sh`: [[view]](./lxplus7/install7.sh) [[download]](https://gitlab.cern.ch/clange/lxplus-like/-/raw/master/lxplus7/install7.sh?inline=false)
- `install8.sh`: [[view]](./lxplus8/install8.sh) [[download]](https://gitlab.cern.ch/clange/lxplus-like/-/raw/master/lxplus8/install8.sh?inline=false)
- `install9.sh`: [[view]](./lxplus9/install9.sh) [[download]](https://gitlab.cern.ch/clange/lxplus-like/-/raw/master/lxplus9/install9.sh?inline=false)

Provide them as "Customization Script" in the Configuration tab.
Alternatively, use the openstack CLI as detailed below and provide them as
additional `--user-data` parameter.

More information on customisation can found in the
[Contextualisation docs](https://clouddocs.web.cern.ch/using_openstack/contextualisation.html).

## VM creation using openstack CLI

The openstack CLI is available on `lxplus8`. You can also install it on your
own machine, e.g. on MacOS using `brew`:

```shell
brew install openstackclient
```

Make sure to download `openrc` file from
[this link](https://openstack.cern.ch/project/api_access/openrc/)
and then source it (asks for CERN account password).

List available images:

```shell
openstack image list
```

List available machine sizes ("flavours"):

```shell
openstack flavor list
```

Show existing machines:

```shell
openstack server list
```

List details of existing machine (here `clangevm-1`):

```shell
openstack server show clangevm-1
```

To debug, you can list e.g. the last 20 lines from the console:

```shell
openstack console log show --line 20 clangevm-1
```

### Examples

Set the following variables first, replacing `lxplus` by the desired key name
listed under
[OpenStack key pairs](https://openstack.cern.ch/project/key_pairs)
and replacing `clangevm-1` by your desired VM hostname:

```shell
export KEY_NAME="lxplus"
export VM_HOST_NAME="clangevm-1"
```

AlmaLinux 9 machine:

Without user-data (i.e. run Ansible playbooks afterwards):

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.large --image 'ALMA9 - x86_64' ${VM_HOST_NAME}
```

With user-data:

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.large --image 'ALMA9 - x86_64' --user-data ./lxplus9/install9.sh ${VM_HOST_NAME}
```

AlmaLinux 8 machine:

Without user-data (i.e. run Ansible playbooks afterwards):

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.large --image 'ALMA8 - x86_64' ${VM_HOST_NAME}
```

With user-data:

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.large --image 'ALMA8 - x86_64' --user-data ./lxplus8/install8.sh ${VM_HOST_NAME}
```

CentOS CERN 7 machine (here medium flavour):

Without user-data (i.e. run Ansible playbooks afterwards):

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.medium --image 'CC7 - x86_64 [2023-04-01]' ${VM_HOST_NAME}
```

With user-data:

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.medium --image 'CC7 - x86_64 [2023-04-01]' --user-data ./lxplus7/install7.sh ${VM_HOST_NAME}
```

The received user-data can be seen from the respective machine using the
following command:

```shell
curl http://169.254.169.254/latest/user-data
```

### Using cloud-init

You can also provide a cloud-config, see examples in the
[cloudinit docs](https://cloudinit.readthedocs.io/en/latest/reference/examples.html).
The cloudinit version on CC7 is very old, so only configs for Alma8 and 9 are
provided:

- [Alma8 `cloud-config`](./lxplus8/cloud-config)
- [Alma9 `cloud-config`](./lxplus9/cloud-config)

Example for use with Alma9:

```shell
openstack server create --key-name ${KEY_NAME} --flavor m2.large --image 'ALMA9 - x86_64' --user-data ./lxplus9/cloud-config ${VM_HOST_NAME}
```

Debugging `cloud-init` can be done by checking the logs at
`/var/log/cloud-init.log`.
To run `cloud-init` again, delete the following two files:

```shell
rm /var/lib/cloud/instances/*/boot-finished
rm /var/lib/cloud/instances/*/sem/config_scripts_user
```

Then run the final module again:

```shell
cloud-init modules --mode final
```

The `cloud-config` file can be found at
`/var/lib/cloud/instances/*/cloud-config.txt`.
