#!/bin/sh
# Pass this as --user-data to the openstack server create command
# alternatively the script can be uploaded via the configuration tab
# of the "Launch Instance" panel of OpenStack
/usr/bin/yum update -y
/usr/bin/yum install -y tmux zsh yum-cron yum-utils
sed -i -e '/apply_updates/ s/no/yes/' /etc/yum/yum-cron.conf

tee /etc/cron.daily/0autoreboot.cron <<EOF
#!/bin/sh
needs-restarting -r >/dev/null || { reboot; exit 0; }
needs-restarting -s | grep -q auditd.service && { reboot; exit 0; }
needs-restarting -s | xargs --no-run-if-empty -n1 systemctl restart
EOF

chmod +x /etc/cron.daily/0autoreboot.cron
systemctl start yum-cron
for i in afs cvmfs eosclient kerberos lpadmin ntp sendmail ssh sudo; do
  locmap --enable $i
done
locmap --configure all
/usr/bin/yum -y install \
  apptainer \
  bc \
  binutils \
  bzip2 \
  bzip2-libs \
  CERN-CA-certs \
  cern-wrappers \
  cmake3 \
  compat-libstdc++-33 \
  cvs \
  e2fsprogs \
  e2fsprogs-libs \
  file \
  file-libs \
  gcc \
  gcc \
  gcc-c++ \
  gcc-c++ \
  gfal2-util \
  gfal2-all \
  git \
  git-lfs \
  glibc-devel \
  glibc-devel.i686 \
  glibc-headers \
  glx-utils \
  hostname \
  java-1.8.0-openjdk \
  java-1.8.0-openjdk-devel \
  krb5-devel \
  krb5-workstation \
  libaio \
  libgfortran \
  libSM \
  libX11 \
  libX11-devel \
  libXcursor \
  libXext \
  libXext-devel \
  libXft \
  libXft-devel \
  libXi \
  libXinerama \
  libXmu \
  libXpm \
  libXpm-devel \
  libXrandr \
  libXrender \
  make \
  mesa-dri-drivers \
  mesa-libGL-devel \
  mesa-libGLU-devel \
  nano \
  nspr \
  nss \
  nss-util \
  openssl \
  openssl-devel \
  patch \
  pciutils \
  perl-Data-Dumper \
  perl-Env \
  perl-ExtUtils-Embed \
  perl-libwww-perl \
  perl-Storable \
  perl-Switch \
  perl-Thread-Queue \
  popt \
  python-pip \
  python-requests-kerberos \
  python2-psutil \
  python3 \
  readline \
  readline-devel \
  strace \
  svn \
  tar \
  tcl \
  tcl-devel \
  time \
  tk \
  tk-devel \
  unzip \
  voms-clients-cpp \
  wget \
  which \
  xrootd-client \
  yum-plugin-ovl \
  zip \
  zsh

echo user.max_user_namespaces=15000 > /etc/sysctl.d/90-max_user_namespaces.conf
sysctl -p /etc/sysctl.d/90-max_user_namespaces.conf
/usr/bin/yum install -y https://repo.opensciencegrid.org/osg/3.6/osg-3.6-el7-release-latest.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-OSG-2
/usr/bin/yum install -y osg-ca-certs
rpm --import https://linuxsoft.cern.ch/wlcg/RPM-GPG-KEY-wlcg
/usr/bin/yum install -y https://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm
/usr/bin/yum install -y HEP_OSlibs wlcg-voms-cms

curl -L -o cvmfs.tar.gz 'https://gitlab.cern.ch/clange/lxplus-like/-/archive/master/lxplus-like-master.tar.gz?path=cvmfs'
tar xzf cvmfs.tar.gz
rsync -r lxplus-like-master-cvmfs/cvmfs /etc
rm -rf lxplus-like-master-cvmfs cvmfs.tar.gz
systemctl restart autofs
