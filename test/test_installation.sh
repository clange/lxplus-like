#!/bin/zsh
# Run as follows:
# ssh clange@clangevm-1.cern.ch 'sh -s' < ./test/test_installation.sh
set -ex

ls /afs/cern.ch/user/c/clange > /dev/null
ls /eos/user/c/clange > /dev/null
set +x
source /cvmfs/cms.cern.ch/cmsset_default.sh
set -x
cmssw-cc7
exit
klist
echo $HOME |grep afs
file /cvmfs/cms.cern.ch/SITECONF/local | grep "symbolic link to T2_CH_CERN"
gfal-copy --help > /dev/null
